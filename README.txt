Overview
--------

Enables your users to place a bet on virtually anything; you decide
which bets you offer. After the outcome of the bet offer is published,
the module automatically calculates the amount of points each user
gets.

The module is compatible with Drupal 6.x


Features
--------

* Node based, integrates well with Drupal
  A bet is constructed as a node, so you can use all node-related
  goodies such as comments, URL path setting, authoring information,
  revisions, or taxonomy.

* Secure code
  The module is carefully designed to avoid possible XSS attacks
  or SQL injections.

* Fully themeable
  The looks of bets will fit into your website theme without
  further tweaking.

* True cross database support
  Every SQL statement conforms to the Core SQL-99 standard.
  Therefore, this module is compatible with every database
  that supports that standard, including (but not limited to)
  MySQL and PostgreSQL.


Author
------

Tobias Quathamer <t.quathamer@gmx.net>
