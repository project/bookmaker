<?php
/*
 * Copyright (C) 2006-2008 Tobias Quathamer <t.quathamer@gmx.net>
 *
 * This file is part of Bookmaker.
 *
 * Bookmaker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bookmaker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bookmaker; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @file
 * Helper functions for the bookmaker module
 */

/**
 * Convert a string into an associative array
 *
 * This function is used to perform the conversion from the stored
 * points string in the database to an array, which can be used for
 * generating the edit form or calculation purposes.
 *
 * Format:
 * string "exact=5\ntendency=15"
 *
 * Result:
 * array('exact' => 5, 'tendency' => 15)
 */
function _bookmaker_betoffer_points_array($point_string) {
  $result = array();

  // each key/value pair is expected to be on a single line
  $lines = split("\n", $point_string);
  foreach ($lines as $line) {
    // there can only be one equal sign on a line
    $equal_sign = strpos($line, '=');
    if ($equal_sign !== FALSE) {
      // there is at least one equal sign in the line, so get the key
      $key = trim(substr($line, 0, $equal_sign));
      $value = (int) substr($line, $equal_sign + 1);
      // if the value is 0, skip the assignment
      if ($value != 0) {
        $result[$key] = $value;
      }
    }
  }

  return $result;
}



/**
 * Convert an associative array into a string
 *
 * This function is used to perform the conversion from a submitted
 * array to a points string, suitable for storing in the database.
 *
 * Format:
 * array('exact' => 5, 'tendency' => 15)
 *
 * Result:
 * string "exact=5\ntendency=15"
 */
function _bookmaker_betoffer_points_string($point_array) {
  $result = array();
  foreach ($point_array as $key => $value) {
    // don't include normal indexes
    if (!is_integer($key)) {
      // if the value is 0, skip the assignment
      if ((int)$value != 0) {
        $result[] = $key ."=". (int)$value;
      }
    }
  }
  return join("\n", $result);
}



/**
 * Calculate the points every user gets for their bet on a bet offer
 */
function bookmaker_calculate_user_points($outcome, $user_bets, $points) {
  $user_points = array();

  // cycle through every user bet
  foreach ($user_bets as $uid => $bet) {
    // make sure the user gets a fresh start with the points
    $user_points[$uid] = 0;

    // calculate the points for an exact match
    if ($points['exact'] != 0) {
      if ($outcome == $bet) {
        $user_points[$uid] += $points['exact'];
      }
    }

    // calculate the points for a correct tendency
    // this is aimed at team sports such as soccer. the current divider
    // to be used is the colon, e.g. outcome = 4:3, bet = 3:1
    if ($points['tendency'] != 0) {
      // don't assign points to a bet equalling the outcome
      // also, only start calculation if there is a colon
      if ($outcome != $bet && strpos($outcome, ':') > 0) {
        // split the outcome by the colon
        $split = explode(':', $outcome);
        $outcome_a = $split[0];
        $outcome_b = $split[1];

        // split the bet by the colon
        $split = explode(':', $bet);
        $bet_a = $split[0];
        $bet_b = $split[1];

        if ($outcome_a > $outcome_b && $bet_a > $bet_b) {
          // this is a correct tendency like 4:2 -> 6:0
          $user_points[$uid] += $points['tendency'];
        }
        else if ($outcome_a < $outcome_b && $bet_a < $bet_b) {
          // this is a correct tendency like 2:4 -> 0:6
          $user_points[$uid] += $points['tendency'];
        }
        else if ($outcome_a == $outcome_b && $bet_a == $bet_b) {
          // this is a correct tendency like 3:3 -> 1:1
          $user_points[$uid] += $points['tendency'];
        }
      }
    }
  }

  return $user_points;
}
